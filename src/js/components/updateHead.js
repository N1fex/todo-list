﻿import $ from 'jquery'

let completedTasks = $('.tasks-completed');
let pendingTasks = $('.tasks-pending');

export default () => {
  completedTasks.html(tasks.filter(task => task.completed).length);
  pendingTasks.html(tasks.filter(task => !task.completed).length);
}