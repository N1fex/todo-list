import $ from 'jquery'
import '@/style/app.scss'

import Task from './components/Task'
import updateHead from './components/updateHead'

$(document).ready(function(){
  window.tasks = [];
  window.circles = [];

  // start project
  async function initProject(){
    let data = await fetch('https://run.mocky.io/v3/a781d65f-1460-47da-a982-5a1bbfdd09fa');
    tasks = await data.json();
    showTasksDOM();
  }

  initProject();

  let tasksBlock = $('.tasks');

  let addTaskButton = $('.add-task');

  let dataTask = $('.data-task');

  let createButton = $('.create');

  let cancelButton = $('.cancel');

  let titleInput = $('.input-title');

  let showTasksDOM = () => {
    tasksBlock.html('');
    $(tasks).each(async (i, obj) => {
      let task = new Task(obj);
      await tasksBlock.append(task.render());
      task.functionality();
    });

    updateHead();
  }

  let newTask = () => {
    let task = tasks[tasks.length-1];
    task = new Task(task)
    tasksBlock.append(task.render());
    task.functionality()
    updateHead();
  }

  addTaskButton.on("click", function(){
    $(this).removeClass('active');
    dataTask.addClass('active');
  });

  createButton.on("click", function(){
    addTask(titleInput.val().trim());
    clearInputs();
  });

  cancelButton.on("click", function(){
    $(this).closest(dataTask).removeClass('active');
    addTaskButton.addClass('active');
  });

  let addTask = (title) => {
    if (!!title.length){
      tasks.push({
        id: +new Date,
        title,
        completed: false
      });
      newTask();

      createButton.closest(dataTask).removeClass('active');
      addTaskButton.addClass('active');
    } else {
      dataTask.addClass('active');
    }
  }

  let clearInputs = () => {
    titleInput.val('');
  }
})